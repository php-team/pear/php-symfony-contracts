<?php
// require_once 'Psr/Cache/autoload.php'; (already required by Cache)
// require_once 'Psr/Container/autoload.php'; (already required by Service)
// require_once 'Psr/EventDispatcher/autoload.php'; (already required by EventDispatcher)

require_once __DIR__ . '/Cache/autoload.php';
require_once __DIR__ . '/Deprecation/autoload.php';
require_once __DIR__ . '/EventDispatcher/autoload.php';
require_once __DIR__ . '/HttpClient/autoload.php';
require_once __DIR__ . '/Service/autoload.php';
require_once __DIR__ . '/Translation/autoload.php';

// if (stream_resolve_include_path('Symfony/Component/Cache/autoload.php')){ (already suggested by Cache)
//     include_once 'Symfony/Component/Cache/autoload.php';
// }
// if (stream_resolve_include_path('Symfony/Component/EventDispatcher/autoload.php')){ (already suggested by EventDispatcher)
//     include_once 'Symfony/Component/EventDispatcher/autoload.php';
// }
// if (stream_resolve_include_path('Symfony/Component/HttpClient/autoload.php')){ (already suggested by HttpClient)
//     include_once 'Symfony/Component/HttpClient/autoload.php';
// }
// if (stream_resolve_include_path('Symfony/Component/DependencyInjection/autoload.php')){ (already suggested by Service)
//     include_once 'Symfony/Component/DependencyInjection/autoload.php';
// }
// if (stream_resolve_include_path('Symfony/Component/Translation/autoload.php')){ (already suggested by Translation)
//     include_once 'Symfony/Component/Translation/autoload.php';
// }
